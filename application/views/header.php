<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <title>Arcade GKW</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('plantilla/');?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/templatemo-cyborg-gaming.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/owl.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/animate.css">
    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('plantilla/');?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/isotope.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/owl-carousel.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/tabs.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/popup.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/custom.js"></script>
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <!-- Api maps -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-cunRm6r-G4XEg0wLrw3AHPbQJSxb8fw&libraries=places&callback=initMap"></script>
    <!-- Data tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <!-- jquery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/additional-methods.min.js" integrity="sha512-6S5LYNn3ZJCIm0f9L6BCerqFlQ4f5MwNKq+EthDXabtaJvg3TuFLhpno9pcm+5Ynm6jdA9xfpQoMz2fcjVMk9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  </head>

<body>
  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->



  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="<?php echo site_url('Welcome/index');?>" class="logo">
                        <img src="<?php echo base_url('plantilla/');?>assets/images/logogkw.png" alt="">
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li><a href="<?php echo site_url('Welcome/index');?>">Inicio</a></li>
                        <li><a href="<?php echo site_url('Torneos/index');?>">Torneos</a></li>
                        <li><a href="/gkw-web/index.php/juegos/index">Videojuegos</a></li>
                        <li><a href="/gkw-web/index.php/calendarios/index">Calendarios</a></li>
                        <li><a href="<?php echo site_url('Jugadores/index');?>" class="active">Jugadores</a></li>
                        <li><a href="<?php echo site_url('Premios/index');?>" class="active">Premios</a></li>
                        <li><a href="<?php echo site_url('Locales/index');?>" class="active">Locales</a></li>
                        <li><a href="<?php echo site_url('Welcome/logout');?>">Cerrar Sesión <img src="<?php echo base_url('plantilla/');?>assets/images/profile-header.png" alt=""></a></li>                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->
