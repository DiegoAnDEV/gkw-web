<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-content">
        <!-- Separación del contenedor principal -->
        <!-- Inicio del Banner -->
        <div class="main-banner">
					<div class="row">
						<div class="col-lg-7">
							<div class="header-text">
								<h6>Torneos</h6>
								<h4><em>Torneos</em> Realizados Y En Curso</h4>
							</div>
						</div>
					</div>
				</div>
        <!-- Fin del banner -->
        <div class="most-popular">
					<div class="row">
						<div class="col-lg-12">
							<div class="heading-section">
								<h4><em>Todos</em> Los Torneos</h4>
							</div>
							<div class="row">
                <!-- Aqui va el item del torneo -->
                <?php foreach ($torneos as $torneoTemporal): ?>
                <div class="col-lg-3 col-sm-6">
									<div class="item">
										<img src="<?php echo base_url('plantilla/');?>assets/images/popular-01.jpg" alt="">
										<h4><?php echo $torneoTemporal->nombre_tor?><br><span><?php echo $torneoTemporal->descripcion_tor?>
                      <ul>
  											<li><i class="fa fa-star"></i> <?php echo $torneoTemporal->fecha_tor?></li>
  											<li><i class="fa fa-download"></i> <?php echo $torneoTemporal->estado_tor?></li>
                        <li><i class="fa fa-pen"></i> <a href="<?php echo site_url();?>/Torneos/editar/<?php echo $torneoTemporal->id_tor?>">Editar Torneo</a></li>
                        <li><i class="fa fa-xmark"></i> <a href="<?php echo site_url();?>/Torneos/eliminar/<?php echo $torneoTemporal->id_tor?>"">Eliminar Torneo</a></li>
  										</ul></span></h4>
									</div>
								</div>
                <?php endforeach; ?>
                <!-- Aqui termina el item del torneo -->
								<div class="col-lg-12">
									<div class="main-button">
										<a href="<?php echo site_url('Torneos/nuevo');?>">Añadir Torneo Nuevo</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        <!-- Separación del Contenedor principal -->
      </div>
    </div>
  </div>
</div>
