<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="page-content">
        <!-- Separación del container -->
        <form class=""
          id="form_nuevo_torneo"
          action="<?php echo site_url();?>/Torneos/guardar"
          method="post">
          <!-- Aqui se define el tipo de método para el input, este es de tipo post -->
          <h1>Ingreso de Nuevo Torneo</h1>
            <div class="row">
              <div class="col-md-6">
                  <label for="" style="color:white">Nombre del Torneo:</label>
                  <br>
                  <input type="text"
                  placeholder="Ingrese un nombre"
                  class="form-control"
                  name="nombre_tor" value=""
                  required
                  id="nombre_tor">
              </div>
              <div class="col-md-6">
                <label for="descripcion_tor" style="color:white">Descripción:</label>
                <br>
                <input type="text"
                placeholder="Ingrese una descripción"
                class="form-control"
                name="descripcion_tor" value=""
                required
                id="descripcion_tor">
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-6">
                  <label for="" style="color:white">Estado:</label>
                  <br>
                  <select class="form-select" aria-label="Default select example" name="estado_tor" id="estado_tor" required>
                    <option selected></option>
                    <option value="Activo">Activo</option>
                    <option value="Inactivo">Inactivo</option>
                  </select>
              </div>
              <div class="cold-md-6" style="width:50%">
                <label for="" style="color:white">Fecha del Torneo: </label>
                <br>
                <input name="fecha_tor" id="fecha_tor" class="form-control" type="date" value="" required>
              </div>
            <br>
            <!-- Botones -->
            <div class="row">
              <hr>
                <div class="col-md-12 text-center">
                    <button type="submit" name="button"
                    class="btn btn-primary">
                      Guardar
                    </button>
                    &nbsp;
                    <a href="<?php echo site_url('Torneos/index');?>"class="btn btn-danger">Cancelar</a>
                </div>
            </div>

            <script type="text/javascript">
            $("#form_nuevo_torneo").validate({
              rules:{
                  nombre_tor:{
                    required:true,
                    minlength:5,
                    maxlength:100,
                  },
                  descripcion_tor:{
                    required:true,
                    minlength:15,
                    maxlength:300,
                  },
                  estado_tor:{
                    required:true
                  },
                  fecha_tor:{
                    required:true,
                    minlength:10,
                    maxlength:10
                  }
              },
              messages:{
                nombres_tor:{
                  required:"Nombre faltante",
                  minlength: "Ingrese un nombre válido",
                  maxlength: "Ingrese un nombre válido",
                },
                descripcion_tor:{
                  required:"Descripción faltante",
                  minlength:"Ingrese una descripción válida (Mínimo 15 caracteres)",
                  maxlength: "Ingrese una descripción válida (Máximo 300 caracteres)",
                },
                estado_tor:{
                  required:"Estado faltante"
                },
                fecha_tor:{
                  required:"Fecha Faltante"
                }
              }
            });
            </script>

          </form>
        <!-- Separación del container -->
      </div>
    </div>
  </div>
</div>
