<br>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE CALENDARIOS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('calendarios/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar calendario
    </a>
</div>
<br>
<br>
<br>
<?php if ($calendario) : ?>
    <table class="table table=striped table-bordered table-hover" id="tbl_calendario">
        <thead>
            <tr >
                <th><font color=red;>CODIGO</font></th>
                <th><font color=red;>DESCRIPCION</font></th>
                <th><font color=red;>FECHA</font></th>
                <th><font color=red;>HORA</font></th>
                <th><font color=red;>ACCIONES</font></th>
            </tr>
        </thead>
        <tbody style>
            <?php foreach ($calendario
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <font color=#F70B85;> <?php echo
                        $filaTemporal->cod_calen; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85;><?php echo
                        $filaTemporal->descripcion_calen; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85> <?php echo
                        $filaTemporal->fecha_calen; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85><?php echo
                        $filaTemporal->hora_calen; ?></font>
                    </td>
                    
                    <td class="text-center">
                        <a href="#" title="Editar Juego">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/calendarios/eliminar/<?php echo $filaTemporal->cod_calen; ?>" title="Eliminar juego" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"><font color=#F70B85>>eliminar</font></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have calendario<h1>
        <?php endif; ?>

<script type="text/javascript"> $("#tbl_calendario").DataTable();</script>