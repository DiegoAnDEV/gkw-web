
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h1 style="color:red;"><b>NUEVO CALENDARIO</b></h1>
            <br>
            <br>
        </div>
    </div>
</div>

<div class="container">
    <form class="" action="<?php echo site_url(); ?>/calendarios/guardar" method="post">
        <div class="row">
            <div class="col-md-4 text-center font">
                <label for=""><font color="red">CODIGO:</font></label> 
                <br>
                <input type="text" placeholder="Ingrese el codigo del calendario" class="form-control" name="cod_calen" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""> <font color="red">DESCRIPCION DEL CALENDARIO:</font></label>
                <br>
                <input type="text" placeholder="Ingrese la descripcion del calendario" class="form-control" name="descripcion_calen" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""><font color="red">FECHA:</label></font>
                <br>
                <input type="text" placeholder="Ingrese la fecha" class="form-control" name="fecha_calen" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""><font color="red">HORA:</label></font>
                <br>
                <input type="text" placeholder="Ingrese la hora" class="form-control" name="hora_calen" value="">
            </div>            
        </div>
        <br>
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                GUARDAR
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/calendarios/index" class="btn btn-danger">CANCELAR</a>
        </div>
        <br>

    </form>
</div>
<br>
<br>