<br>
<body style="background-image: url(<?php echo base_url();?>/assets/images/fondo.jpg);width:100%;height:100%;">
  <div class="container"  >
    <div class="row" >
      <div class="col-lg-12">
        <div class="page-content" style="background-color: rgba(0, 0, 0, 0.7)"  >


          <form class=""
          id="form_nuevo"
          action="<?php echo site_url();?>/Premios/guardar"
          method="post" >
          <!-- Aqui se define el tipo de método para el input, este es de tipo post -->

          <h1 class="text-center" style="color:white">NUEVO PREMIO</h1>
          <br>
              <div class="row">
                <div class="col-md-4">
                    <label for="" style="color:white">Nombre:</label>
                    <br>
                    <input type="text"
                    placeholder="Ingrese nombre de premio"
                    class="form-control"
                    name="nombre_pre" value=""
                    id="nombre_pre">
                </div>
                <div class="col-md-8">
                  <label for="" style="color:white">Descripcion:</label>
                  <br>
                  <input type="text"
                  placeholder="Ingrese una decripcion del premio"
                  class="form-control"
                  name="descripcion_pre" value=""
                  id="descripcion_pre">
                </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <label for="" style="color:white">Fecha:</label>
                    <br>
                    <input type="date"
                    placeholder="Ingrese fecha de entrega"
                    min="2023-06-02" max="2024-06-02"
                    class="form-control"
                    name="fecha_pre" value=""
                    id="fecha_pre">
                  </div>
                </div>
                  &nbsp;&nbsp;&nbsp;
                  <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button"
                        class="btn btn-primary">
                          Guardar
                        </button>
                        &nbsp;
                        <a href="<?php echo site_url();?>/premios/index"class="btn btn-danger">Cancelar</a>
                    </div>
                </div>

              </div>

              </div>
              <br>


              <script type="text/javascript">


              $("#form_nuevo").validate({
                rules:{
                    nombre_pre:{
                      required:true,
                      minlength:3,
                      maxlength:250,
                    },
                    descripcion_pre:{
                      required:true,
                      minlength:3,
                      maxlength:250,
                    },
                    fecha_pre:{
                      date:true,
                      required:true,
                      minlength:2023-06-02,
                      maxlength:2024-06-02,
                    },
                },
                messages:{
                  nombre_pre:{
                    required:"Por favor ingrese nombre",
                    lettersonly: "Solo letras",
                    minlength:"Ingrese un nombre válido",
                    maxlength:"Ingrese un nombre válido"
                  },
                  fecha_pre:{
                    date:"Ingrese fecha valida",
                    required:"Por favor ingrese Descripcion",
                    minlength:"Ingrese fecha actual",
                    maxlength:"Fecha fuera de rango"
                  },
                }
              });
              </script>

          </form>


        </div>

      </div>

    </div>


  </div>
</body>
<br>
