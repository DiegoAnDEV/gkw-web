<body style="background-image: url(<?php echo base_url();?>/assets/images/fond.jpg);width:100%;height:100%;">
  <div class="container">
    <div class="row">
      <div class="page-content"  style="background-color: rgba(0, 0, 0, 0.7)">
        <?php if ($premios): ?>
          <h2 class="text-center">PREMIOS</h2>
          <div class="col-md-12">
            <a href="<?php echo site_url();?>/premios/nuevo"class="btn btn-danger">NUEVO</a>
          </div>

          &nbsp;&nbsp;&nbsp;&nbsp;
          <table class="table table-bordered table-hover" style="color:white; background-color: #110325" id="tablaPremios">
            <br>
            <thead class="" style="background-color:black">
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DESCRIPCION</th>
                <th>FECHA</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($premios as $filatemporal): ?>
                <tr>
                  <td>
                    <?php echo $filatemporal->id_pre?>
                  </td>
                  <td>
                    <?php echo $filatemporal->nombre_pre?>
                  </td>
                  <td>
                    <?php echo  $filatemporal->descripcion_pre?>
                  </td>
                  <td>
                    <?php echo  $filatemporal->fecha_pre?>
                  </td>



                  <td class="text-center">
                    <a href="<?php echo site_url();?>/premios/editar/<?php echo $filatemporal->id_pre?>" title="Editar Premio" style="color:green">
                      <i class="fa fa-pencil"></i> Editar
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url();?>/premios/eliminar/<?php echo $filatemporal->id_pre?>" title="Eliminar Premio" style="color:red">
                      <i class="mdi mdi-close"></i> Eliminar
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php
          // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
         ?>
        <?php else: ?>
        <h1>No hay datos</h1>
        <?php endif; ?>
      </div>
    </div>
  </div>
</body>

<script type="text/javascript">
  $("#tablaPremios").DataTable();
</script>
