<br>
<body style="background-image: url(<?php echo base_url();?>/assets/images/fondo.jpg);width:100%;height:100%;">
  <div class="container"  >
    <div class="row" >
      <div class="col-lg-12">
        <div class="page-content" style="background-color: rgba(0, 0, 0, 0.5)"  >


          <form class=""
          id="form_nuevo"
          action="<?php echo site_url();?>/Locales/guardar"
          method="post" >
          <!-- Aqui se define el tipo de método para el input, este es de tipo post -->

          <h1 class="text-center" style="color:white">NUEVO LOCAL</h1>
          <br>
              <div class="row">
                <div class="col-md-6">
                    <label for="" style="color:white">Nombre:</label>
                    <br>

                    <input type="text" required placeholder="Ingrese sus nombres" class="form-control"  name="nombre_loc" value=""
                    id="nombre_loc">
                </div>
                <div class="col-md-6">
                  <label for="" style="color:white">Celular:</label>
                  <br>
                  <input type="tel"
                  placeholder="Ingrese numero de celular"
                  class="form-control"
                  name="celular_loc" value=""
                  id="celular_loc">
                </div>
              </div>
              <br>
              <div class="row">

                <div class="col-md-6">
                      <label style="color:white">Latitud</label>
                      <br>
                      <input type="text"
                      placeholder="Ingrese latitud"
                      class="form-control"
                      readonly
                      name="latitud_loc" value=""
                      id="latitud_loc">
                </div>
                <div class="col-md-6">
                      <label style="color:white">Longitud</label>
                      <br>
                      <input type="text"
                      placeholder="Ingrese latitud"
                      readonly
                      class="form-control"
                      name="longitud_loc" value=""
                      id="longitud_loc">
                </div>
              </div>
              <br>
              <div class="row">
            <div class="col-md-12">
              <div class="" id="mapaUbicacion1" style="width:100%; height:500px; border:3px solid WHITE;">

              </div>

            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" name="button"
                    class="btn btn-primary">
                      Guardar
                    </button>
                    &nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url();?>/locales/index"class="btn btn-danger">Cancelar</a>
                </div>
            </div>

          </div>


              </div>
              <br>


          <script type="text/javascript">
              $("#form_nuevo").validate({
                rules:{
                    nombre_loc:{
                      required:true,
                      lettersonly: true,
                      minlength:3,
                      maxlength:250,
                    },
                    celular_loc:{
                      number:true,
                      required:true,
                      minlength:10,
                      maxlength:10,
                    }
                  },
                messages:{
                  nombre_loc:{
                    required:"Por favor ingrese nombre",
                    lettersonly: "Solo letras",
                    minlength:"Ingrese un nombre válido",
                    maxlength:"Ingrese un nombre válido"
                  },
                   celular_loc:{
                     number:"Ingrese solo números",
                    required:"Por favor, ingrese número de celular",
                    minlength:"Número Incorrecto, ingrese 10 digitos",
                    maxlength:"Número Incorrecto, ingrese 10 digitos"
                  },
                }

              });
              </script>
              <script type="text/javascript">
              function initMap(){
                var centro= new google.maps.LatLng(-0.9320643505121627, -78.61401199542358);
                var mapa2=new google.maps.Map(
                  document.getElementById('mapaUbicacion1'),
                  {
                    center:centro,
                    zoom:7,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                  }
                );
                var marcador=new google.maps.Marker({
                position:centro,
                title:"Seleccione la direccion",
                map:mapa2,
                draggable:true
              });
              google.maps.event.addListener(marcador,'dragend',function(){
                document.getElementById('latitud_loc').value=this.getPosition().lat();
                document.getElementById('longitud_loc').value=this.getPosition().lng();
              });
              }

              </script>


          </form>


        </div>

      </div>

    </div>


  </div>
</body>
<br>
