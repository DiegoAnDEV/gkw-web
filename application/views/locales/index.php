<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-cunRm6r-G4XEg0wLrw3AHPbQJSxb8fw&libraries=places&callback=initMap"></script>
<body style="background-image: url(<?php echo base_url();?>/assets/images/fondo1.jpg);width:100%;height:100%;">
  <div class="container">
    <div class="row">
      <div class="page-content"  style="background-color: rgba(0, 0, 0, 0.7)">
        <?php if ($locales): ?>
          <h2 class="text-center">LOCALES</h2>
          <br>

          <div class="row">
      			<div class="col-md-12">
      				<div  id="mapaLugares" style="height:500px; width:100%; border:3px solid WHITE;">

      				</div>

      			</div>

      		</div>
          <br>
          <div class="col-md-12">
            <a href="<?php echo site_url();?>/locales/nuevo"class="btn btn-danger">NUEVO</a>
          </div>

          &nbsp;&nbsp;&nbsp;&nbsp;
          <table class="table table-bordered table-hover" style="color:white; background-color: #110325" id="tablaLocales">

            <br>
            <thead class="" style="background-color:black">
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>CELULAR</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($locales as $filatemporal): ?>
                <tr>
                  <td>
                    <?php echo $filatemporal->id_loc?>
                  </td>
                  <td>
                    <?php echo $filatemporal->nombre_loc?>
                  </td>
                  <td>
                    <?php echo  $filatemporal->celular_loc?>
                  </td>
                  <td>
                    <?php echo  $filatemporal->latitud_loc?>
                  </td>
                  <td>
                    <?php echo  $filatemporal->longitud_loc?>
                  </td>

                  <td class="text-center">
                    <a href="<?php echo site_url();?>/locales/editar/<?php echo $filatemporal->id_loc?>" title="Editar Local" style="color:green">
                      <i class="fa fa-pencil"></i> Editar
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url();?>/locales/eliminar/<?php echo $filatemporal->id_loc?>" title="Eliminar Local" style="color:red">
                      <i class="mdi mdi-close"></i> Eliminar
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php
          // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
         ?>
        <?php else: ?>
        <h1>No hay datos</h1>
        <?php endif; ?>


      </div>

    </div>

  </div>


  <script type="text/javascript">
  $("#tablaLocales").DataTable();
  			function initMap(){
  				var centro= new google.maps.LatLng(-1.509536, -79.398414);
  				var mapaLugaresTuristicos=new google.maps.Map(
  					document.getElementById('mapaLugares'),
  					{
  						center:centro,
  						zoom:7,
  						mapTypeId:google.maps.MapTypeId.ROADMAP
  					}
  				);
  				<?php if ($locales):?>
  				<?php foreach($locales as $lugarTemporal): ?>
  				var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_loc;?>,<?php echo $lugarTemporal->longitud_loc; ?>);
  				var marcador=new google.maps.Marker({
  				position:coordenadaTemporal,
  				title:"<?php echo $lugarTemporal->nombre_loc; ?>",
  				map:mapaLugaresTuristicos,
          draggable:true
  			});
  			<?php endforeach; ?>
  			<?php endif; ?>
  			}
  		</script>

</body>
