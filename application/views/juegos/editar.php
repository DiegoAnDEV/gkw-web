<h1>EDITAR JUEGO</h1>
<form class="" id="frm_editar_juego" 
action="OJO" method="post">
    <div class="row">
        <input type="text" name="id_ins" id="id_ins"
        value="<?php echo $indexEditar->id_vid; ?>">
        <div class="col-md-4">
            <label for="">CÉDULA:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="number" 
            placeholder="Ingrese la cédula" 
            class="form-control" 
            required 
            min="99999999" 
            name="cedula_ins" value="<?php echo $instructorEditar->cedula_ins; ?>" 
            id="cedula_ins">
        </div>
        <div class="col-md-4">
            <label for="">Primer Apellido:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" 
            placeholder="Ingrese el primer apellido" 
            class="form-control" 
            required 
            name="primer_apellido_ins" 
            value="<?php echo $instructorEditar->primer_apellido_ins; ?>" 
            id="primer_apellido_ins">
        </div>
        <div class="col-md-4">
            <label for="">Segundo Apellido:</label>
            <br>
            <input type="text" 
            placeholder="Ingrese el segundo apellido" 
            class="form-control" name="segundo_apellido_ins" 
            value="<?php echo $instructorEditar->segundo_apellido_ins; ?>" 
            id="segundo_apellido_ins">
        </div>
        <div class="col-md-4">
            <label for="">Nombres:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" 
            placeholder="Ingrese sus nombres" 
            required 
            class="form-control" 
            name="nombres_ins" 
            value="<?php echo $instructorEditar->nombres_ins; ?>" 
            id="nombres_ins">
        </div>
        <div class="col-md-4">
            <label for="">Título:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su título" 
            required class="form-control" 
            name="titulo_ins" 
            value="<?php echo $instructorEditar->titulo_ins; ?>" 
            id="titulo_ins">
        </div>
        <div class="col-md-4">
            <label for="">Teléfono:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="number" placeholder="Ingrese el teléfono" 
            class="form-control" 
            required name="telefono_ins" 
            value="<?php echo $instructorEditar->telefono_ins; ?>" 
            id="telefono_ins">
        </div>
        <div class="col-md-12">
            <label for="">Dirección:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" 
            placeholder="Ingrese la dirección" required 
            class="form-control" 
            name="direccion_ins" 
            value="<?php echo $instructorEditar->direccion_ins; ?>" id="direccion_ins">
        </div>
    </div>
    <br>
    <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
            Editar
        </button>
        &nbsp;
        <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">CANCELAR</a>
    </div>
    <br>
</form>
<script type="text/javascript">
    $("#frm_editar_instructor").validate({
        rules: {
            cedula_ins: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
            primer_apellido_ins: {
                required: true,
                minlength: 3,
                maxlength: 250,
                letras: true
            },
            segundo_apellido_ins: {
                letras: true
            },
            nombres_ins: {
                required: true
            },
            titulo_ins: {
                required: true
            },
            telefono_ins: {
                required: true,
                digits: true
            },
            direccion_ins: {
                required: true
            }
        },
        messages: {
            cedula_ins: {
                required: "POR FAVOR INGRESE EL NUMERO DE CEDULA",
                minlength: "CEDULA INCORRECTA, INGRESE 10 DIGITOS",
                maxlength: "CEDULA INCORRECTA, INGRESE 10 DIGITOS",
                digits: "SOLO SE ACEPTA NUMEROS",
                number: "SOLO SE ACEPTA NUMEROS"
            },
            primer_apellido_ins: {
                required: "POR FAVOR INGRESE EL PRIMER APELLIDO",
                minlength: "POR FAVOR INGRESE EL PRIMER APELLIDO",
                maxlength: "POR FAVOR INGRESE EL PRIMER APELLIDO"
            },
            nombres_ins: {
                required: "POR FAVOR INGRESE EL NOMBRE"
            },
            titulo_ins: {
                required: "POR FAVOR INGRESE EL TITULO"
            },
            telefono_ins: {
                required: "POR FAVOR INGRESE SOLO NUMEROS",
                digits: "POR FAVOR INGRESE SOLO NUMEROS"
            },
            direccion_ins: {
                required: "POR FAVOR INGRESE LA DIRRECCION"
            }

        }
    });
</script>