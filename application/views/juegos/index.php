<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE VIDEOJUEGOS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('juegos/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Videojuego
    </a>
</div>
<br>
<br>
<br>
<?php if ($videojuego) : ?>
    <table class="table table=striped table-bordered table-hover" id="tbl_videojuegos">
        <thead>
            <tr >
                <th><font color=red;>ID</font></th>
                <th><font color=red;>NOMBRE</font></th>
                <th><font color=red;>DESCRIPCION</font></th>
                <th><font color=red;>CATEGORIA</font></th>
                <th><font color=red;>ACCIONES</font></th>
            </tr>
        </thead>
        <tbody style>
            <?php foreach ($videojuego
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <font color=#F70B85;> <?php echo
                        $filaTemporal->id_vid; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85;><?php echo
                        $filaTemporal->nombre_vid; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85> <?php echo
                        $filaTemporal->descripcion_vid; ?></font>
                    </td>
                    <td>
                        <font color=#F70B85><?php echo
                        $filaTemporal->categoria_vid; ?></font>
                    </td>
                    
                    <td class="text-center">
                        <a href="#" title="Editar Juego">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/juegos/eliminar/<?php echo $filaTemporal->id_vid; ?>" title="Eliminar juego" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"><font color=#F70B85>>eliminar</font></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have juegos<h1>
        <?php endif; ?>



<script type="text/javascript"> $("#tbl_videojuegos").DataTable();</script>