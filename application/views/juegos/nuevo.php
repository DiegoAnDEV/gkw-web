
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h1 style="color:red;"><b>NUEVO JUEGO</b></h1>
            <br>
            <br>
        </div>
    </div>
</div>

<div class="container">
    <form class="" action="<?php echo site_url(); ?>/juegos/guardar" method="post">
        <div class="row">
            <div class="col-md-4 text-center font">
                <label for=""><font color="red">ID JUEGO:</font></label> 
                <br>
                <input type="text" placeholder="Ingrese el id del juego" class="form-control" name="id_vid" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""> <font color="red">NOMBRE DEL JUEGO:</font></label>
                <br>
                <input type="text" placeholder="Ingrese el nombre del juego" class="form-control" name="nombre_vid" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""><font color="red">DESCRIPCION:</label></font>
                <br>
                <input type="text" placeholder="Ingrese la descripcion" class="form-control" name="descripcion_vid" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for=""><font color="red">CATEGORIA:</label></font>
                <br>
                <input type="text" placeholder="Ingrese la categoria al que pertenece el juego" class="form-control" name="categoria_vid" value="">
            </div>            
        </div>
        <br>
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                GUARDAR
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/juegos/index" class="btn btn-danger">CANCELAR</a>
        </div>
        <br>

    </form>
</div>
<br>
<br>