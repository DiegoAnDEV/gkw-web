<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <title>Arcade GKW</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('plantilla/');?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/templatemo-cyborg-gaming.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/owl.css">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/');?>assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('plantilla/');?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/isotope.min.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/owl-carousel.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/tabs.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/popup.js"></script>
    <script src="<?php echo base_url('plantilla/');?>assets/js/custom.js"></script>
    <!-- jquery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/additional-methods.min.js" integrity="sha512-6S5LYNn3ZJCIm0f9L6BCerqFlQ4f5MwNKq+EthDXabtaJvg3TuFLhpno9pcm+5Ynm6jdA9xfpQoMz2fcjVMk9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  </head>

<body>
  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-content">
          <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?php echo site_url('welcome/login');?>">Administrador</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?php echo site_url('welcome/loginJugador');?>">Jugador</a>
          </li>
        </ul>

          <form class=""
            action="<?php echo site_url();?>/Welcome/iniciarSesion"
            method="post">
            <!-- Aqui se define el tipo de método para el input, este es de tipo post -->
            <h1 class="text-center">Login de Jugador</h1>
              <div class="row">
                <div class="col-md-6">
                    <label for="" style="color:white">Correo:</label>
                    <br>
                    <input type="text"
                    placeholder="Ingrese su correo"
                    class="form-control"
                    name="correo_jug" value=""
                    required
                    id="correo_jug">
                    <br>
                    <label for="" style="color:white">Contraseña:</label>
                    <br>
                    <input type="password"
                    placeholder="Ingrese su contraseña"
                    class="form-control"
                    name="contrasena_jug" value=""
                    required
                    id="contrasena_jug">
                </div>
                <div class="col-md-6 text-center">
                  <img src="<?php echo base_url('plantilla/assets/images/gkwlogo.png');?>" alt="" style="width:200px;">
                </div>
              </div>
              <br>
              <div class="row">
              <br>
              <!-- Botones -->
              <div class="row">
                <hr>
                  <div class="col-md-12 text-center">
                      <button type="submit" name="button"
                      class="btn btn-primary">
                        Ingresar
                      </button>
                      &nbsp;
                      <a href="<?php echo site_url('Welcome/loginJugador');?>"class="btn btn-danger">Cancelar</a>
                  </div>
              </div>
            </form>

        </div>
      </div>
    </div>
  </div>
