<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="page-content">

				<!-- ***** Banner Start ***** -->
				<div class="main-banner">
					<div class="row">
						<div class="col-lg-7">
							<div class="header-text">
								<h6>Bienvenido a GKW <?php echo $data->usuario_adm;?></h6>
								<h4><em>El Mejor</em> Sitio Para Gamers</h4>
							</div>
						</div>
					</div>
				</div>
				<!-- ***** Banner End ***** -->

				<!-- ***** Most Popular Start ***** -->
				<!-- <div class="most-popular">
					<div class="row">
						<div class="col-lg-12">
							<div class="heading-section">
								<h4><em>Juegos Más Populares</em> Ahora Mismo</h4>
							</div>
							<div class="row">
								<div class="col-lg-3 col-sm-6">
									<div class="item">
										<img src="<?php echo base_url('plantilla/');?>assets/images/popular-01.jpg" alt="">
										<h4>Fortnite<br><span>Sandbox</span></h4>
										<ul>
											<li><i class="fa fa-star"></i> 4.8</li>
											<li><i class="fa fa-download"></i> 2.3M</li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-sm-6">
									<div class="item">
										<img src="<?php echo base_url('plantilla/');?>assets/images/popular-02.jpg" alt="">
										<h4>PubG<br><span>Battle S</span></h4>
										<ul>
											<li><i class="fa fa-star"></i> 4.8</li>
											<li><i class="fa fa-download"></i> 2.3M</li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-sm-6">
									<div class="item">
										<img src="<?php echo base_url('plantilla/');?>assets/images/popular-03.jpg" alt="">
										<h4>Dota2<br><span>Steam-X</span></h4>
										<ul>
											<li><i class="fa fa-star"></i> 4.8</li>
											<li><i class="fa fa-download"></i> 2.3M</li>
										</ul>
									</div>
								</div>
								<div class="col-lg-3 col-sm-6">
									<div class="item">
										<img src="<?php echo base_url('plantilla/');?>assets/images/popular-04.jpg" alt="">
										<h4>CS-GO<br><span>Legendary</span></h4>
										<ul>
											<li><i class="fa fa-star"></i> 4.8</li>
											<li><i class="fa fa-download"></i> 2.3M</li>
										</ul>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="main-button">
										<a href="#">Más Videojuegos</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<!-- ***** Most Popular End ***** -->
			</div>
		</div>
	</div>
</div>
