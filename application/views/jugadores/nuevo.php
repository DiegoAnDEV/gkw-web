<br>
<body style="background-image: url(<?php echo base_url();?>/assets/images/fondo.jpg);width:100%;height:100%;">
  <div class="container"  >
    <div class="row" >
      <div class="col-lg-12">
        <div class="page-content" style="background-color: rgba(0, 0, 0, 0.5)"  >


          <form class=""
          id="form_nuevo"
          action="<?php echo site_url();?>/Jugadores/guardar"
          method="post" >
          <!-- Aqui se define el tipo de método para el input, este es de tipo post -->

          <h1 class="text-center" style="color:white">NUEVO JUGADOR</h1>
          <br>
              <div class="row">
                <div class="col-md-4">
                    <label for="" style="color:white">Cédula:</label>

                    <input type="number" placeholder="Ingrese su cédula" class="form-control" name="cedula_jug" value=""
                    required
                    id="cedula_jug"   <span class="obligatorio">(obligatorio)</span>
                </div>
                <div class="col-md-4">
                    <label for="" style="color:white">Nombre:</label>
                    <br>
                    <input type="text"
                    placeholder="Ingrese sus nombres"
                    class="form-control"
                    name="nombre_jug" value=""
                    id="nombre_jug">
                </div>
                <div class="col-md-4">
                  <label for="" style="color:white">Apellido:</label>
                  <br>
                  <input type="text"
                  placeholder="Ingrese el apellido"
                  class="form-control"
                  name="apellido_jug" value=""
                  id="apellido_jug">
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                    <label for="" style="color:white">Correo:</label>
                    <br>
                    <input type="email"
                    placeholder="email@ejemplo.com"
                    class="form-control"
                    name="correo_jug" value=""
                    id="correo_jug">
                </div>
                <div class="col-md-4">
                      <label style="color:white">Ingrese Contraseña:</label>
                      <div class="input-group">
                    <input ID="txtPassword" type="Password" Class="form-control" value=""  name="contrasena_jug" title="Una contraseña válida es una cadena con una longitud entre 8 y 20 caracteres, donde cada uno consiste en una letra mayúscula o minúscula, un dígito, o los símboloss '@', '#', '$' y '%'"
                   id="contrasena_jug">
                    <div class="input-group-append">
                          <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                        </div>
                        <p style="color:red;">Click en el ojo para mostrar / ocultar</p>
                  </div>

                </div>
              </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-md-12 text-center">
                      <button type="submit" name="button"
                      class="btn btn-primary">
                        Guardar
                      </button>
                      &nbsp;
                      <a href="<?php echo site_url();?>/jugadores/index"class="btn btn-danger">Cancelar</a>
                  </div>
              </div>

              <script type="text/javascript">

              function mostrarPassword(){
          		var cambio = document.getElementById("txtPassword");
          		if(cambio.type == "password"){
          			cambio.type = "text";
          			$('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
          		}else{
          			cambio.type = "password";
          			$('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
          		}
          	}
              $("#form_nuevo").validate({
                rules:{
                  cedula_jug:{
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:10,
                  },
                    nombre_jug:{
                      required:true,
                      lettersonly: true,
                      minlength:3,
                      maxlength:250,
                    },
                    apellido_jug:{
                      required:true,
                      lettersonly: true,
                      minlength:3,
                      maxlength:250,
                    },
                    correo_jug:{
                      required:true,
                      email:true,
                      minlength:3,
                      maxlength:250,

                    }
                },
                messages:{
                  cedula_jug:{
                    required:"Número de cédula faltante",
                    number:"Ingrese un número valido",
                    minlength:"Cédula Incorrecta, ingrese 10 digitos",
                    maxlength:"Cédula Incorrecta, ingrese 10 digitos"
                  },
                  nombre_jug:{
                    required:"Por favor ingrese su nombre",
                    lettersonly: "Solo letras",
                    minlength:"Ingrese un nombre válido",
                    maxlength:"Ingrese un nombre válido"
                  },
                  apellido_jug:{
                    required:"Por favor ingrese  primer apellido",
                    lettersonly: "Solo letras",
                    minlength:"Ingrese un apellido válido",
                    maxlength:"Ingrese un apellido válido"
                  },
                  correo_jug:{
                    required:"Por favor ingrese su correo",
                    email:"Ingrese un correo valido ejemplo: ejem@gmail.com",
                    minlength:"Ingrese un correo válido",
                    maxlength:"Ingrese un correo válido",
                  }
                }
              });
              </script>

          </form>


        </div>

      </div>

    </div>


  </div>
</body>
<br>
