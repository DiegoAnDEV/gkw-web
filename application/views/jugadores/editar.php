<div class="container">
  <div class="row">
    <div class="page-content">
      <form class=""
      id="form_editar"
      action="<?php echo site_url('jugadores/procesarActualizacion');?>"
      method="post">
      <!-- Aqui se define el tipo de método para el input, este es de tipo post -->
      <h1>EDITAR JUGADOR</h1>
      <div class="row">
            <div class="col-md-12">
              <label for="">Id: </label>
              <input type="hidden" name="id_jug" id=id_jug value="<?php echo $jugadorEditar->id_jug;?>">
            </div>

            </div>
          <div class="row">
            <div class="col-md-4">
                <label for="">Cédula:</label>
                <br>
                <input type="number" placeholder="Ingrese la cédula" class="form-control" name="cedula_jug"
                value="<?php echo  $jugadorEditar->cedula_jug;?>"
                required
                id="cedula_jug"   <span class="obligatorio">(obligatorio)</span>
            </div>
            <div class="col-md-4">
                <label for="">Nombre:</label>
                <br>
                <input type="text"
                placeholder="Ingrese sus nombres"
                class="form-control"
                name="nombre_jug" value="<?php echo  $jugadorEditar->nombre_jug;?>"
                id="nombre_jug">
            </div>
            <div class="col-md-4">
              <label for="">Apellido:</label>
              <br>
              <input type="text"
              placeholder="Ingrese el apellido"
              class="form-control"
              name="apellido_jug" value="<?php echo  $jugadorEditar->apellido_jug;?>"
              id="apellido_jug">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
                <label for="">Correo:</label>
                <br>
                <input type="text"
                placeholder="email@ejemplo.com"
                class="form-control"
                name="correo_jug" value="<?php echo  $jugadorEditar->correo_jug;?>"
                id="correo_jug">
            </div>
            <div class="col-md-4">
                <label for="">Contrasena:</label>
                <br>
                <input type="password"
                placeholder="Ingrese su contrasena"
                class="form-control"
                name="contrasena_jug" value="<?php echo  $jugadorEditar->contrasena_jug;?>"
                id="contrasena_jug">
            </div>
          </div>
          <br>
          <div class="row">
              <div class="col-md-12 text-center">
                  <button type="submit" name="button"
                  class="btn btn-primary">
                    Guardar
                  </button>
                  &nbsp;
                  <a href="<?php echo site_url();?>/jugadores/index"class="btn btn-danger">Cancelar</a>
              </div>
          </div>

          <script type="text/javascript">

          $("#form_editar").validate({
            rules:{
              cedula_jug:{
                required:true,
                number:true,
                minlength:10,
                maxlength:10,
              },
                nombre_jug:{
                  required:true,
                  lettersonly: true,
                  minlength:3,
                  maxlength:250,
                },
                apellido_jug:{
                  required:true,
                  lettersonly: true,
                  minlength:3,
                  maxlength:250,
                },
                correo_jug:{
                  required:true,
                  email:true,
                  minlength:3,
                  maxlength:250,

                }
            },
            messages:{
              cedula_jug:{
                required:"Número de cédula faltante",
                number:"Ingrese un número valido",
                minlength:"Cédula Incorrecta, ingrese 10 digitos",
                maxlength:"Cédula Incorrecta, ingrese 10 digitos"
              },
              nombre_jug:{
                required:"Por favor ingrese su nombre",
                lettersonly: "Solo letras",
                minlength:"Ingrese un nombre válido",
                maxlength:"Ingrese un nombre válido"
              },
              apellido_jug:{
                required:"Por favor ingrese  primer apellido",
                lettersonly: "Solo letras",
                minlength:"Ingrese un apellido válido",
                maxlength:"Ingrese un apellido válido"
              },
              correo_jug:{
                required:"Por favor ingrese su correo",
                email:"Ingrese un correo valido ejemplo: ejem@gmail.com",
                minlength:"Ingrese un correo válido",
                maxlength:"Ingrese un correo válido",
              }
            }
          });
          </script>

      </form>


    </div>

  </div>

</div>
</div>
