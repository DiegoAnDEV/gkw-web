<body style="background-image: url(<?php echo base_url();?>/assets/images/fondo3.jpg);width:100%;height:100%;">
  <div class="container">
    <div class="row">
      <div class="page-content"  style="background-color: rgba(0, 0, 0, 0.7)">
        <?php if ($jugadores): ?>
          <h2 class="text-center">JUGADORES</h2>
          <div class="col-md-12">
            <a href="<?php echo site_url();?>/jugadores/nuevo"class="btn btn-danger">NUEVO</a>
          </div>

          &nbsp;&nbsp;&nbsp;&nbsp;
          <table class="table table-bordered table-hover" style="color:white; background-color: #110325 " id="tbl_jugadores">

            <br>
            <thead class="" style="background-color:black">
              <tr>
                <th>ID</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Correo</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($jugadores as $filatemporal): ?>
                <tr>
                  <td>
                    <?php echo $filatemporal->id_jug?>
                  </td>
                  <td>
                    <?php echo $filatemporal->cedula_jug?>
                  </td>
                  <td>
                    <?php echo $filatemporal->nombre_jug?>
                  </td>
                  <td>
                    <?php echo $filatemporal->apellido_jug?>
                  </td>
                  <td>
                    <?php echo $filatemporal->correo_jug?>
                  </td>

                  <td class="text-center">
                    <a href="<?php echo site_url();?>/jugadores/editar/<?php echo $filatemporal->id_jug?>" title="Editar Jugador" style="color:green">
                      <i class="fa fa-pencil"></i> Editar
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url();?>/jugadores/eliminar/<?php echo $filatemporal->id_jug?>" title="Eliminar Jugador" style="color:red">
                      <i class="mdi mdi-close"></i> Eliminar
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php
          // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
         ?>
        <?php else: ?>
        <h1>No hay datos</h1>
        <?php endif; ?>


      </div>

    </div>

  </div>
  <script type="text/javascript">
$("#tbl_jugadores").DataTable();

</script>
</body>
