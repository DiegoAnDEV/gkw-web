<?php
  class Jugador extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevoJugador){
        return $this->db->insert("jugador",$datosNuevoJugador);
    }

    function obtenerTodos(){
      $listadoJugadores=$this->db->get('jugador');
      if ($listadoJugadores->num_rows()>0) {
        return $listadoJugadores->result();
      } else {
        return false;
      }
    }
    function borrar($id_jug){
      $this->db->where('id_jug',$id_jug);
      return $this->db->delete('jugador');
    }
    // Función para consultar where
    function obtenerID($id_jug){
      $this->db->where('id_jug',$id_jug);
      $jugador=$this->db->get('jugador');
      if ($jugador->num_rows()>0) {
        return $jugador->row();
      }else{
        return false;
      }
    }

    //funcion para actualizar un jugador
    function actualizar($id_jug,$datosEditados)
    {
      $this->db->where('id_jug',$id_jug);
      return $this->db->update('jugador',$datosEditados);
    }
  }//Cierre de la clase
 ?>
