<?php
  class Torneo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function obtenerTorneos(){
      $listadoTorneos=$this->db->get('torneo');
      if ($listadoTorneos->num_rows()>0) {
        return $listadoTorneos->result();
      } else {
        return false;
      }
    }
    function insertar($datosNuevoTorneo){
        return $this->db->insert("torneo",$datosNuevoTorneo);
    }
    function borrar($id_tor){
      $this->db->where('id_tor',$id_tor);
      return $this->db->delete('torneo');
    }
    function obtenerID($id_tor){
      $this->db->where('id_tor',$id_tor);
      $torneo=$this->db->get('torneo');
      if ($torneo->num_rows()>0) {
        return $torneo->row();
      }else{
        return false;
      }
    }
    function actualizar($id_tor,$datosEditados)
    {
      $this->db->where('id_tor',$id_tor);
      return $this->db->update('torneo',$datosEditados);
    }

  }//Aqui termina la clase
?>
