<?php
  class Local extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevoLocal){
        return $this->db->insert("local",$datosNuevoLocal);
    }

    function obtenerTodos(){
      $listadoLocales=$this->db->get('local');
      if ($listadoLocales->num_rows()>0) {
        return $listadoLocales->result();
      } else {
        return false;
      }
    }
    function borrar($id_loc){
      $this->db->where('id_loc',$id_loc);
      return $this->db->delete('local');
    }
    // Función para consultar where
    function obtenerID($id_loc){
      $this->db->where('id_loc',$id_loc);
      $local=$this->db->get('local');
      if ($local->num_rows()>0) {
        return $local->row();
      }else{
        return false;
      }
    }


    //funcion para actualizar un jugador
    function actualizar($id_loc,$datosEditados)
    {
      $this->db->where('id_loc',$id_loc);
      return $this->db->update('local',$datosEditados);
    }



  }//Cierre de la clase
 ?>
