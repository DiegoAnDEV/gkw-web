<?php
  class Premio extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datosNuevoPremio){
        return $this->db->insert("premio",$datosNuevoPremio);
    }

    function obtenerTodos(){
      $listadoPremios=$this->db->get('premio');
      if ($listadoPremios->num_rows()>0) {
         return $listadoPremios->result();
      

      } else {
        return false;
      }
    }
    function borrar($id_pre){
      $this->db->where('id_pre',$id_pre);
      return $this->db->delete('premio');
    }
    // Función para consultar where
    function obtenerID($id_pre){
      $this->db->where('id_pre',$id_pre);
      $premio=$this->db->get('premio');
      if ($premio->num_rows()>0) {
        return $premio->row();
      }else{
        return false;
      }
    }


    //funcion para actualizar un jugador
    function actualizar($id_pre,$datosEditados)
    {
      $this->db->where('id_pre',$id_pre);
      return $this->db->update('premio',$datosEditados);
    }



  }//Cierre de la clase
 ?>
