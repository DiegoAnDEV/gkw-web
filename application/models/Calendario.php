<?php

    class Calendario extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un servicio
        function insertar($datos){
            return $this->db->insert("calendario", $datos);

        }
        //Funcion para consultar servicios
        function obtenerTodos(){
            $listadoCalendario=
            $this->db->get("calendario");
            
            if($listadoCalendario->
            num_rows()>0){//Si hay datos
                    return $listadoCalendario->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar servicio
        function borrar($cod_calen){
            $this->db->where("cod_calen",$cod_calen);
            return $this->db->delete("calendario"); 
        }
    }//Cierre de la clase
?>