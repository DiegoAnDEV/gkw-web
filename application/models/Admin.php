<?php
  class Admin extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    function obtenerEmailPassword($usuario,$password){
      $this->db->where('usuario_adm',$usuario);
      $this->db->where('contrasena_adm',$password);
      $admin=$this->db->get('admin');
      if ($admin->num_rows()>0) {
        return $admin->row();
      }else{
        return false;
      }
    }
  }
?>
