<?php

    class Juego extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un servicio
        function insertar($datos){
            return $this->db->insert("videojuego", $datos);

        }
        //Funcion para consultar servicios
        function obtenerTodos(){
            $listadoJuegos=
            $this->db->get("videojuego");
            
            if($listadoJuegos
                ->num_rows()>0){//Si hay datos
                    return $listadoJuegos->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar servicio
        function borrar($id_vid){
            $this->db->where("id_vid",$id_vid);
            return $this->db->delete("videojuego"); 
        }
    }//Cierre de la clase
?>