<?php
class Calendarios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Calendario');
    }
    //Funcion que renderiza la vista index                                        
    public function index()
    {
        $data['calendario'] = $this->Calendario->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('calendarios/index',$data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('calendarios/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoCalendario = array(
            "cod_calen" => $this->input->post('cod_calen'),
            "descripcion_calen" => $this->input->post('descripcion_calen'),
            "fecha_calen" => $this->input->post('fecha_calen'),
            "hora_calen" => $this->input->post('hora_calen')
        );
        if ($this->Calendario->insertar($datosNuevoCalendario)) {
            $this->session->set_flashdata("confirmacion", "Calendario guarado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al guardar revise e intete nuevamente");
        }
        redirect('calendarios/index');
    }
    //funcion para eliminar juego
    public function eliminar($cod_calen)
    {
        if ($this->Calendario->borrar($cod_calen)) {
            $this->session->set_flashdata("confirmacion", "Calendario eliminado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al eliminar");
        }
        redirect('calendarios/index');
    }    
} // Cierre de la clase