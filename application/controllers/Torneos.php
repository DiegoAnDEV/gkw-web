<?php
  class Torneos extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Torneo');
      if (!$this->session->userdata("conectado") ) {
        redirect("Welcome/login");
      }
    }
    public function index()
    {
      $data['torneos']=$this->Torneo->obtenerTorneos();
      $this->load->view('header');
      $this->load->view('torneos/index',$data);
      $this->load->view('footer');
    }
    public function nuevo()
    {
      $this->load->view('header');
      $this->load->view('torneos/nuevo');
      $this->load->view('footer');
    }
    public function guardar(){
      //Creación de un array asociativo, que va a guardar en cada espacio un this input
      $datosNuevoTorneo = array(
        'nombre_tor' =>$this->input->post('nombre_tor'),
        'fecha_tor' =>$this->input->post('fecha_tor'),
        'descripcion_tor' =>$this->input->post('descripcion_tor'),
        'estado_tor' =>$this->input->post('estado_tor')
      );
      if ($this->Torneo->insertar($datosNuevoTorneo)) {
        $this->session->set_flashdata("ingreso","Torneo guardado correctamente");
        redirect('torneos/index');
      } else {
        $this->session->set_flashdata("error","Error al guardar, intente otra vez");
      }
    }
    public function eliminar($id_tor){
      if ($this->Torneo->borrar($id_tor)) {
        $this->session->set_flashdata('borrado','Registro eliminado exitosamente');
        redirect('torneos/index');
      } else {
        $this->session->set_flashdata("error","Error al guardar, intente otra vez");
      }
    }
    public function editar($id_tor)
    {
      $data["torneoEditar"]=$this->Torneo->obtenerID($id_tor);
      $this->load->view('header');
  		$this->load->view('torneos/editar',$data);
  		$this->load->view('footer');
    }
    public function procesarActualizacion()
    {
      $datosEditados = array(
        'nombre_tor' =>$this->input->post('nombre_tor'),
        'fecha_tor' =>$this->input->post('fecha_tor'),
        'descripcion_tor' =>$this->input->post('descripcion_tor'),
        'estado_tor' =>$this->input->post('estado_tor')
      );
      $id_tor=$this->input->post('id_tor');
      if ($this->Torneo->actualizar($id_tor,$datosEditados)) {
        $this->session->set_flashdata("editado","Torneo editado con éxito");
        redirect('torneos/index');
      } else {
        $this->session->set_flashdata("error","Error al guardar, intente otra vez");
      }
    }

  }//Aqui se cierra la clase
?>
