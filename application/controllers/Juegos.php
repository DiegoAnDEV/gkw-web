<?php
class Juegos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Juego');
    }
    //Funcion que renderiza la vista index                                        
    public function index()
    {
        $data['videojuego'] = $this->Juego->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('juegos/index',$data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('juegos/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoJuego = array(
            "id_vid" => $this->input->post('id_vid'),
            "nombre_vid" => $this->input->post('nombre_vid'),
            "descripcion_vid" => $this->input->post('descripcion_vid'),
            "categoria_vid" => $this->input->post('categoria_vid')
        );
        if ($this->Juego->insertar($datosNuevoJuego)) {
            $this->session->set_flashdata("confirmacion", "Juego guarado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al guardar revise e intete nuevamente");
        }
        redirect('juegos/index');
    }
    //funcion para eliminar juego
    public function eliminar($id_vid)
    {
        if ($this->Juego->borrar($id_vid)) {
            $this->session->set_flashdata("confirmacion", "Juego eliminado exitosamente");                
        } else {
            $this->session->set_flashdata("error", "Error al eliminar");
        }
        redirect('juegos/index');
    }    
} // Cierre de la clase