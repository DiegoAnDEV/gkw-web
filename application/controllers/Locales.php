<?php
class Locales extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Local');
    }
    //Funcion que renderiza la vista index
    public function index()
    {
        $data['locales'] = $this->Local->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('locales/index',$data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('locales/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoLocal = array(
          'nombre_loc' => $this->input->post('nombre_loc'),
          'celular_loc' => $this->input->post('celular_loc'),
          'latitud_loc' => $this->input->post('latitud_loc'),
          'longitud_loc' => $this->input->post('longitud_loc')
        );
        if ($this->Local->insertar($datosNuevoLocal)) {
          $this->session->set_flashdata("confirmacion","registro guardado Exitosamente");
      } else {
        $this->session->set_flashdata("error","error al guardar intente otra vez");

      }
      redirect('locales/index');
    }
    //funcion para eliminar instructores
    public function eliminar($id_loc){
          echo $id_loc;
          if ($this->Local->borrar($id_loc)) {
            $this->session->set_flashdata("confirmacion","Registro eliminado Exitosamente");
          } else {
            $this->session->set_flashdata("error","Error al eliminar intente otra vez");
          }
          redirect('locales/index');
        }

    public function editar($id_loc)
    {
      $data["localEditar"]=$this->Local->obtenerID($id_loc);
      $this->load->view('header');
      $this->load->view('locales/editar',$data);
      $this->load->view('footer');
    }
//Funcion para renderizar vista editar
    public function procesarActualizacion(){
      $datosEditados = array(
        'nombre_loc' => $this->input->post('nombre_loc'),
        'celular_loc' => $this->input->post('celular_loc'),
        'latitud_loc' => $this->input->post('latitud_loc'),
        'longitud_loc' => $this->input->post('longitud_loc')

      );
        $id_loc=$this->input->post("id_loc");
        if ($this->Local->actualizar($id_loc,$datosEditados))
      {
        $this->session->set_flashdata("confirmacion","Registro actualizado Exitosamente");

        // code...
          } else {
        $this->session->set_flashdata("error","Error al actualizar intente otra vez");
        // code...

        }
      redirect("Locales/index");
      }
} // Cierre de la clase
?>
