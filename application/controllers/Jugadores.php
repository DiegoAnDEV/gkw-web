<?php
class Jugadores extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Jugador');
    }
    //Funcion que renderiza la vista index
    public function index()
    {
        $data['jugadores'] = $this->Jugador->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('jugadores/index',$data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('jugadores/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoJugador = array(
          'cedula_jug' => $this->input->post('cedula_jug'),
          'nombre_jug' => $this->input->post('nombre_jug'),
          'apellido_jug' => $this->input->post('apellido_jug'),
          'correo_jug' => $this->input->post('correo_jug'),
          'contrasena_jug' => $this->input->post('contrasena_jug')
        );
        if ($this->Jugador->insertar($datosNuevoJugador)) {
          $this->session->set_flashdata("confirmacion","Jugador guardado Exitosamente");
      } else {
        $this->session->set_flashdata("error","error al guardar intente otra vez");

      }
      redirect('jugadores/index');
    }
    //funcion para eliminar instructores
    public function eliminar($id_jug){
          echo $id_jug;
          if ($this->Jugador->borrar($id_jug)) {
            $this->session->set_flashdata("confirmacion","Jugador eliminado Exitosamente");
          } else {
            $this->session->set_flashdata("error","Error al eliminar intente otra vez");
          }
          redirect('jugadores/index');
        }

    public function editar($id_jug)
    {
      $data["jugadorEditar"]=$this->Jugador->obtenerID($id_jug);
      $this->load->view('header');
      $this->load->view('jugadores/editar',$data);
      $this->load->view('footer');
    }
//Funcion para renderizar vista editar
    public function procesarActualizacion(){
      $datosEditados = array(
        'cedula_jug' => $this->input->post('cedula_jug'),
        'nombre_jug' => $this->input->post('nombre_jug'),
        'apellido_jug' => $this->input->post('apellido_jug'),
        'correo_jug' => $this->input->post('correo_jug'),
        'contrasena_jug' => $this->input->post('contrasena_jug')

      );
        $id_jug=$this->input->post("id_jug");
        if ($this->Jugador->actualizar($id_jug,$datosEditados))
      {
        $this->session->set_flashdata("confirmacion","Jugador actualizado Exitosamente");

        // code...
          } else {
        $this->session->set_flashdata("error","Error al actualizar intente otra vez");
        // code...

        }
      redirect("Jugadores/index");
      }
} // Cierre de la clase
?>
