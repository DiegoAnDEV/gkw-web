<?php
class Premios extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Premio');
    }
    //Funcion que renderiza la vista index
    public function index()
    {
        $data['premios'] = $this->Premio->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('premios/index',$data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('premios/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoPremio = array(
          'nombre_pre' => $this->input->post('nombre_pre'),
          'descripcion_pre' => $this->input->post('descripcion_pre'),
          'fecha_pre' => $this->input->post('fecha_pre')
        );
        if ($this->Premio->insertar($datosNuevoPremio)) {
          $this->session->set_flashdata("confirmacion","Premio guardado Exitosamente");
      } else {
        $this->session->set_flashdata("error","error al guardar intente otra vez");

      }
      redirect('premios/index');
    }
    //funcion para eliminar instructores
    public function eliminar($id_pre){
          echo $id_pre;
          if ($this->Premio->borrar($id_pre)) {
            $this->session->set_flashdata("confirmacion","Premio eliminado Exitosamente");
          } else {
            $this->session->set_flashdata("error","Error al eliminar intente otra vez");
          }
          redirect('premios/index');
        }

    public function editar($id_pre)
    {
      $data["premioEditar"]=$this->Premio->obtenerID($id_pre);
      $this->load->view('header');
      $this->load->view('premios/editar',$data);
      $this->load->view('footer');
    }
//Funcion para renderizar vista editar
    public function procesarActualizacion(){
      $datosEditados = array(
        'nombre_pre' => $this->input->post('nombre_pre'),
        'descripcion_pre' => $this->input->post('descripcion_pre'),
        'fecha_pre' => $this->input->post('fecha_pre')

      );
        $id_pre=$this->input->post("id_pre");
        if ($this->Premio->actualizar($id_pre,$datosEditados))
      {
        $this->session->set_flashdata("confirmacion","Premio actualizado Exitosamente");

        // code...
          } else {
        $this->session->set_flashdata("error","Error al actualizar intente otra vez");
        // code...

        }
      redirect("Premios/index");
      }
} // Cierre de la clase
?>
