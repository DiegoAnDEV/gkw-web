<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$data=$this->session->userdata("conectado");
		$this->load->view('header');
		$this->load->view('welcome_message',['data'=>$data]);
		$this->load->view('footer');
		if (!$this->session->userdata("conectado") ) {
			redirect("Welcome/login");
		}
	}
	public function login()
	{
		$this->load->view('login');
		$this->load->view('footer');
	}
	public function loginJugador()
	{
		$this->load->view('loginJugador');
		$this->load->view('footer');
	}
	public function logout()
	{
		$this->session->sess_destroy();
    redirect('Welcome/login');
	}
	public function iniciarSesion(){
   	 $this->load->model("Admin");
   	 $email=$this->input->post("usuario_adm");
   	 $password=$this->input->post("contrasena_adm");
   	 $usuarioConectado=$this->Admin->obtenerEmailPassword($email,$password);
   	 if($usuarioConectado){
   					 $this->session->set_userdata("conectado",$usuarioConectado);
						 $this->session->set_flashdata('logueado','Bienvenido de vuelta : '.$usuarioConectado->usuario_adm);
   					 redirect("welcome/index");
   	 }else{
			 $this->session->set_flashdata('negado','Credenciales incorrectas, vuelva a intentar');
   		 redirect("welcome/login");
   	 }
  }
}
